<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\SalesRepresentative;

class SalesRepresentativeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_sales_representatives_view()
    {
        $response = $this->get('/sales-representatives');
        $response->assertStatus(200);
    }

    public function test_create_sales_representative_view()
    {
        $response = $this->get('/sales-representatives/create');
        $response->assertStatus(200);
    }

    public function test_create_sales_representative()
    {
        $response = $this->post('/sales-representatives/store', [
            'full_name' => 'John Doily',
            'email_address' => 'test2@example.com',
            'telephone' => '0978765643',
            'joined_date' => '2022-03-23',
            'current_routes' => 'Maxers Street',
            'comments' => 'Lorem ipsum is placeholder text commonly used in the graphic.',
        ]);

        $response->assertStatus(405);
    }

    // for updates, same ui is used which is used for create
    public function test_sales_representative_show()
    {
        $salesRepresentative = new SalesRepresentative();
        $salesRepresentative->full_name = "Test Repair Shop";
        $salesRepresentative->email_address = "test4@example.com";
        $salesRepresentative->telephone = "0978765643";
        $salesRepresentative->joined_date = "2022-03-23";
        $salesRepresentative->current_routes = "Maxers Street";
        $salesRepresentative->comments = "Lorem ipsum is placeholder text commonly used in the graphic.";
        $salesRepresentative->save();

        $response = $this->get('/sales-representatives/'.($salesRepresentative->id));
        $salesRepresentative->delete();
        $response->assertStatus(200);
    }
}
