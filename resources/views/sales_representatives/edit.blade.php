@extends('layouts.app')

@section('title', 'Edit Sales Representative')

@section('content')
<div class="row">
    <div class="col-sm-12">
    <div>
        <a class="btn btn-success float-end" href="{{url('sales-representatives')}}">View Team</a>
    </div>

        <form action="{{url('sales-representatives')}}/{{$salesRepresentative->id}}" method="POST">
            <input name="_method" type="hidden" value="PUT">
            @include('sales_representatives.form')
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $("#full_name").val("{{$salesRepresentative->full_name}}");
        $("#email_address").val("{{$salesRepresentative->email_address}}");
        $("#telephone").val("{{$salesRepresentative->telephone}}");
        $("#joined_date").val("{{$salesRepresentative->joined_date}}");
        $("#current_routes").val("{{$salesRepresentative->current_routes}}");
        $("#comments").val("{{$salesRepresentative->comments}}");
    });
</script>
@endsection