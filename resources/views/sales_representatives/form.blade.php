<input type="hidden" name="_token" value="{{ csrf_token() }}">
            
<div class="mb-3 mt-3">
    <label for="full_name" class="form-label">Full Name *</label>
    <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name">
    @error('full_name')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="mb-3 mt-3">
    <label for="email_address" class="form-label">Email Address *</label>
    <input type="email" class="form-control @error('email_address') is-invalid @enderror" id="email_address" id="email_address" name="email_address" required>
    @error('email_address')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="mb-3 mt-3">
    <label for="telephone" class="form-label">Telephone *</label>
    <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone" id="telephone" name="telephone" required>
    @error('telephone')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="mb-3 mt-3">
    <label for="joined_date" class="form-label">Joined Date *</label>
    <input type="date" class="form-control @error('joined_date') is-invalid @enderror" id="joined_date" id="joined_date" name="joined_date" required>
    @error('joined_date')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="mb-3 mt-3">
    <label for="current_routes" class="form-label">Current Routes *</label>
    <select class="form-control @error('current_routes') is-invalid @enderror" id="current_routes" id="current_routes" name="current_routes" required>
        <option>Woxword Street</option>
        <option>Volvo Street</option>
        <option>234 Street</option>
        <option>Maxers Street</option>
        <option>Dravers Street</option>
    </select>
    @error('current_routes')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<div class="mb-3 mt-3">
    <label for="comments" class="form-label">Comments *</label>
    <textarea class="form-control @error('comments') is-invalid @enderror" id="comments" name="comments" required></textarea>
    @error('comments')
        <div class="mt-2 alert alert-danger">{{ $message }}</div>
    @enderror
</div>