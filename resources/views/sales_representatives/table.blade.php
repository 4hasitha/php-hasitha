@extends('layouts.app')

@section('title', 'Sales Team')

@section('content')
<div class="row">
    <div class="col-sm-12">
    @if (session('status'))
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
            {{ session('status') }}
        </div>
    @endif
        <div>
            <a class="btn btn-success float-end" href="{{url('sales-representatives/create')}}">Add New</a>
        </div>
        <nav aria-label="...">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="{{$salesRepresentatives->previousPageUrl()}}">Previous</a>
                </li>
                
                <li class="page-item">
                    <a class="page-link" href="{{$salesRepresentatives->nextPageUrl()}}">Next</a>
                </li>
            </ul>
        </nav>
        Showing {{$salesRepresentatives->firstItem()}} to {{$salesRepresentatives->lastItem()}} of {{$salesRepresentatives->total()}} sales representatives
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Current Route</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

            @foreach($salesRepresentatives as $salesRepresentative)
                <tr>
                    <td>{{$salesRepresentative->id}}</td>
                    <td>{{$salesRepresentative->full_name}}</td>
                    <td>{{$salesRepresentative->email_address}}</td>
                    <td>{{$salesRepresentative->telephone}}</td>
                    <td>{{$salesRepresentative->current_routes}}</td>
                    <td><button onclick="showDetails('{{$salesRepresentative->id}}');" class="btn btn-sm btn-primary">View</button></td>
                    <td><a href="{{url('sales-representatives')}}/{{$salesRepresentative->id}}/edit" class="btn btn-sm btn-warning">Edit</a></td>
                    <td><button onclick="deleteSalesRepresentative('{{$salesRepresentative->id}}');" class="btn btn-sm btn-danger">Delete</button></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" aria-labelledby="viewModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="viewModalLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <table class="table">
            <tr>
                <th>Id</th>
                <td id="tableId"></td>
            </tr>

            <tr>
                <th>Full Name</th>
                <td id="tableName"></td>
            </tr>

            <tr>
                <th>Email Address</th>
                <td id="tableEmail"></td>
            </tr>

            <tr>
                <th>Telephone</th>
                <td id="tableTelephone"></td>
            </tr>

            <tr>
                <th>Joined Date</th>
                <td id="tableJoined"></td>
            </tr>

            <tr>
                <th>Current Routes</th>
                <td id="tableCurrentRoutes"></td>
            </tr>

            <tr>
                <th>Comments</th>
                <td id="tableComments"></td>
            </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
    function deleteSalesRepresentative(salesRepresentativeId){
        var id = salesRepresentativeId;
        $.ajax({
            type: 'DELETE',
            url: "{{url('sales-representatives')}}/"+id,
            data: {
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                alert("Sales representative is deleted successfully.");
                location.reload();
            },
            error: function() {
                console.log(data);
            }
        });
    }

    function showDetails(salesRepresentativeId){
        var id = salesRepresentativeId;
        $.ajax({
            type: 'GET',
            url: "{{url('sales-representatives')}}/"+id,
            success: function (data) {
                $("#viewModalLabel").html(data.full_name);
                $("#tableId").html(data.id);
                $("#tableName").html(data.full_name);
                $("#tableEmail").html(data.email_address);
                $("#tableTelephone").html(data.telephone);
                $("#tableJoined").html(data.joined_date);
                $("#tableCurrentRoutes").html(data.current_routes);
                $("#tableComments").html(data.comments);
                $("#viewModal").modal('show');
            },
            error: function() {
                console.log(data);
            }
        });
    }
</script>
@endsection