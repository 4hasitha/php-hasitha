@extends('layouts.app')

@section('title', 'New Sales Representative')

@section('content')
<div class="row">
    <div class="col-sm-12">
    <div>
        <a class="btn btn-success float-end" href="{{url('sales-representatives')}}">View Team</a>
    </div>

        <form action="{{url('sales-representatives')}}" method="POST">
            @include('sales_representatives.form')

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<script>
    
</script>
@endsection