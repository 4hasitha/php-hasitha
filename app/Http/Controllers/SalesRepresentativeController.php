<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesRepresentative;

class SalesRepresentativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salesRepresentatives = SalesRepresentative::paginate(30);
        return view('sales_representatives.table', compact('salesRepresentatives'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales_representatives.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required|max:255',
            'email_address' => 'required|email|unique:sales_representatives,email_address|max:255',
            'telephone' => 'required|max:255',
            'joined_date' => 'required|date',
            'current_routes' => 'required|max:255',
            'comments' => 'required|max:500',
        ]);

        $salesRepresentative = new SalesRepresentative;
        $salesRepresentative->full_name = $request->full_name;
        $salesRepresentative->email_address = $request->email_address;
        $salesRepresentative->telephone = $request->telephone;
        $salesRepresentative->joined_date = $request->joined_date;
        $salesRepresentative->current_routes = $request->current_routes;
        $salesRepresentative->comments = $request->comments;
        $salesRepresentative->save();

        return redirect('sales-representatives')->with('status','Created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return SalesRepresentative::where('id', $id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesRepresentative = SalesRepresentative::where('id', $id)->first();
        return view('sales_representatives.edit', compact('salesRepresentative'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'full_name' => 'required|max:255',
            'email_address' => 'required|email|max:255',
            'telephone' => 'required|max:255',
            'joined_date' => 'required|date',
            'current_routes' => 'required|max:255',
            'comments' => 'required|max:500',
        ]);

        $salesRepresentative = SalesRepresentative::find($id);
        $salesRepresentative->full_name = $request->full_name;
        $salesRepresentative->email_address = $request->email_address;
        $salesRepresentative->telephone = $request->telephone;
        $salesRepresentative->joined_date = $request->joined_date;
        $salesRepresentative->current_routes = $request->current_routes;
        $salesRepresentative->comments = $request->comments;
        $salesRepresentative->save();

        return redirect('sales-representatives')->with('status','Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SalesRepresentative::find($id)->delete();
        return "Success";
    }
}
