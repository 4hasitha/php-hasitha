<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class SalesRepresentativeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $streets = array("Woxword Street", "Volvo Street", "234 Street", "Maxers Street", "Dravers Street");
        $rand_keys = array_rand($streets, 2);
        return [
            'full_name' => fake()->name(),
            'email_address' => fake()->unique()->safeEmail(),
            'telephone' => '071'.rand(1111111,9999999),
            'joined_date' =>  now(),
            'current_routes' => $streets[$rand_keys[0]],
            'comments' => 'Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.',
        ];
    }
}
